import argparse
import datetime
import string

from util.text import ErrorMsg, ColorsText, ColorsHex

__author__ = 'march'


class NodeCommandParser:
    def __init__(self):
        pass

    def add_sched_actions(self, parser):
        parser.add_argument("-d", dest="delay", type=self.check_delay, help="delay command, use s/m/h/d for units")
        parser.add_argument("-t", dest="timer", type=self.check_timer, help="add timed command, use HH:MM format")

    def parse(self, msg):
        arg_parser = argparse.ArgumentParser(prog="node", description="available node control commands")
        arg_parser.add_argument("id", help="node id to send the command to")

        subparser = arg_parser.add_subparsers(title="commands", help="call with -h for more help")

        # power command subparser
        power_parser = subparser.add_parser("power", help="toggle node power")
        power_parser.add_argument("action", metavar="action", choices=["on", "off"], help="on/off")
        self.add_sched_actions(power_parser)

        # color command subparser
        color_parser = subparser.add_parser("color", help="control node color")
        color_parser.add_argument("color", type=self.check_color, help="color name or hex value")
        self.add_sched_actions(color_parser)
        color_parser.set_defaults(action="color")

        # alias command subparser
        alias_parser = subparser.add_parser("alias", help="set a friendly alias")
        alias_parser.add_argument("alias", help="string alias for a given node")

        # parse the args, turning them into dictionary from a Namespace to ease iteration
        return arg_parser.parse_args(msg)

    def check_color(self, color):
        """Check the color formatting for node color command
        :param color: raw string to check the format
        :return: hex value of a valid color
        """
        # search for the pre-defined colors and return on if found
        for val, col in ColorsText.__members__.items():
            if color == col:
                # FIXME: perhaps not the best way, but how to travers two enums?
                return eval("ColorsHex.{}.value".format(val))

        # if no valid color was specified, try to set the hex value
        # support 6 and 3 digit hex values
        if all(c in string.hexdigits for c in color):
            if len(color) == 6:
                return color
            elif len(color) == 3:
                color *= 2
                return color
        else:
            # raise the argparse error
            msg = "'{}' {}".format(color, ErrorMsg.C_UNKNOWN)
            raise argparse.ArgumentTypeError(msg)

    def check_delay(self, delay):
        # get the units and the delay value
        unit = delay.lstrip("0123456789")

        # TODO: this raises exception, still caught by the parser, but incorrect
        # for example input node 1 timer on will crash here
        if unit not in "":
            value = int(delay[:-len(unit)])
        else:
            return int(delay)

        # manipulate the returned value
        if unit in "s" or "":
            pass
        elif unit in "m":
            value *= 60
        elif unit in "h":
            value *= 3600
        elif unit in "d":
            value *= 86400
        else:
            # raise the argparse error
            msg = "'{}': {}".format(delay, "is not in valid delay format")
            raise argparse.ArgumentTypeError(msg)
        return value

    def check_timer(self, timer):
        """Check the timed action format correctness used with argparse
        :param timer: raw timer string to check
        :return: absolute value for the action to be executed in seconds
        """
        units = list(map(int, timer.split(':')))
        if len(units) == 2:
            value = datetime.datetime.now().replace(hour=units[0], minute=units[1])
            value = value - datetime.datetime.fromtimestamp(0)  # subtract epoch time
        else:
            msg = "'{}': {}".format(timer, "is not in valid time format, try HH:MM")
            raise argparse.ArgumentTypeError(msg)

        return value.total_seconds()


class PresetCommandParser:
    def add_node_options(self, parser):
        parser.add_argument("node", type=int, help="node id to add the preset to")

    def parse(self, msg):
        arg_parser = argparse.ArgumentParser(prog="preset", description="presets control commands")
        subparser = arg_parser.add_subparsers(title="commands", help="call with -h for more help")

        # list command subparser
        list_parser = subparser.add_parser("list", help="list active presets")
        list_parser.set_defaults(action="list")

        # add command subparser
        add_parser = subparser.add_parser("add", help="add a new preset")
        self.add_node_options(add_parser)
        add_parser.set_defaults(action="add")

        # remove command subparser
        add_parser = subparser.add_parser("remove", help="remove a preset")
        self.add_node_options(add_parser)
        add_parser.set_defaults(action="remove")

        return arg_parser.parse_args(msg)

    def check_repetition(self, rep):
        """Check the repetition string format for the preset add argument
        """
        pass


class UICommandParser:
    def __init__(self):
        pass

    def parse(self, msg):
        pass
