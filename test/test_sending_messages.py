import subprocess
import time
from comm import server
from control.node import Node

__author__ = 'march'

import unittest

server_addr = "192.168.0.4"
PORT = 3912


class MessageSendingTest(unittest.TestCase):

    def setUp(self):
        self.srv = server.NodeServer(server_addr, PORT)
        self.srv.start()

    def test_node_message_string_parsing(self):
        # emulate json recv with ncat
        time.sleep(1)
        p1 = subprocess.Popen(["echo", '{"_id":"0x01"}'], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["ncat", "-u", server_addr, str(PORT)], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()

        # send a badly formatted message to node
        #self.srv.send([" ", "   ", "1", "on"])
        nd = Node(1, "0.0.0.0")
        self.assertEqual(nd.get_led(), "on")

    def tearDown(self):
        self.srv.shutdown()

if __name__ == '__main__':
    unittest.main()
