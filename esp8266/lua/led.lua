rgb_pins = {1,2,3}
status = false
cur_col = "000000"
     
function led_init()
     for pin in pairs(rgb_pins) do
          gpio.mode(pin, gpio.OUTPUT)
          pwm.setup(pin, 500, 512)
     end     
end

function led_parse_json(js)
     for field,val in pairs(js) do
          if field == "led" then
               led_set(val)
          elseif field == "color" then
               led_set_color(val)
          else
               print("invalid json: {"..field.." : "..val.."}")
          end
     end
end

function led_hex_to_rgb(x)
     if type(x) == 'number' then
        x = string.format('%x+', x):upper()
     end
   
     x = x:match'%x+':upper()
     if x:len() ~= 6 then
        print('invalid color: ' .. x)
     end
     return {
        r = loadstring('return 0x'..x:sub(1,2))();
        g = loadstring('return 0x'..x:sub(3,4))();
        b = loadstring('return 0x'..x:sub(5,6))();
     }
end

function led_set_color(col)
     local hcolor = led_hex_to_rgb(col)
     
     pwm.setduty(rgb_pins[1], hcolor.r*4)
     pwm.setduty(rgb_pins[2], hcolor.g*4)
     pwm.setduty(rgb_pins[3], hcolor.b*4)
     cur_col = col
     print("color: "..col)
end

function led_set(mode)
     if mode == "on" then
          for pin in pairs(rgb_pins) do
               pwm.start(pin)
               status = true
          end
     else
          for pin in pairs(rgb_pins) do
               pwm.stop(pin)
               status = false
          end
     end
     print("DEBUG: led: "..mode)
end

function led_dim(value)
end

function led_error_flash(times)
    local blinks = times*2
    local restore_col = cur_col
    
    tmr.alarm(1, 100, 1, function()
        if blinks > 0 then

            -- swap led color
            if status == false then
                led_set_color("ff0000")
                status = true
            else
                led_set_color("000000")
                status = false
            end
            blinks = blinks - 1
        
        else 
            -- stop the timer, restore current color
            tmr.stop(1)
            led_set_color(restore_col)
        end
    end)
end