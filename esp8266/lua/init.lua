-- sta config
ssid = "oneplus"
pwd = "truechiptilldeath!"

wifi.setmode(wifi.STATION)
wifi.sta.config(ssid, pwd)

print("--- dbulb project v0.1 ---")
wifi.sta.connect()

tmr.alarm(1, 10000, 1, function() 
    if wifi.sta.getip()== nil then 
         print("waiting for IP") 
    else
         tmr.stop(1)
         print(wifi.sta.getip())
         dofile("main.lua")
    end
end)
