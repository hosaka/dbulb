from util.text import NodeFields, NodeCommands

__author__ = 'march'


class NodeTable():
    table = {}

    def __iter__(self):
        for n in sorted(self.table):
            yield n

    # def list(self):
    #     for n in sorted(self.table):
    #         yield n

    def add(self, node):
        self.table[node.get_id()] = node

    def update(self, node):
        self.add(node)

    def get(self, _id):
        return self.table[_id]

    def get_by_alias(self, al):
        for id in self.table:
            nd = self.table[id]
            if al == nd.get_alias():
                return nd

    def get_by_id_or_alias(self, node):
        if not self.is_empty():
            # search by id if the string is a number
            if node.isdigit() and self.exists(int(node)):
                return self.get(int(node))

            # search by alias otherwise
            else:
                nd = self.get_by_alias(node)
                if nd is not None:
                    return nd

    def remove(self, _id):
        del(self.table[_id])

    def exists(self, _id):
        return True if _id in self.table else False

    def is_empty(self):
        return not self.table

    def amount(self):
        return len(self.table)


class Node():
    # common node fields
    _status = False
    _id = 0x00  # ip can be stored in it's own format, use import address
    _ip = ""
    _alias = "unknown"
    _on_time = 0
    _off_time = 0

    def __init__(self, id, ip):
        self._id = id
        self._ip = ip

    def get_json(self):
        """Return the json dictionary consisting of node fields and values.

        Various nodes include their fields in the resulting json dictionary.
        :return: dictionary with node fields
        """
        json = {
            NodeFields.ID.value: self._id,
            NodeFields.IP.value: self._ip,
            NodeFields.ALIAS.value: self._alias,
            NodeFields.ON_TIME.value: self._on_time,
            NodeFields.OFF_TIME.value: self._off_time
        }
        return json

    def set_from_json(self, new_json):
        """Set node fields from given json dictionary.

        Each node implements own set from json method according
        to its functionality. Traverse the json dictionary and
        ignore invalid fields.
        :param new_json: dictionary to look for fields
        """
        pass

    def get_type(self):
        """Return the type of a particular node in plain text.

        Mainly used for formatting and distinguishing between different
        nodes while using the user interface. Defaults to subclass name
        :return: type string
        """
        return self.__class__.__name__

    def get_id(self):
        return self._id

    def set_id(self, new_id):
        if isinstance(new_id, int):
            self._id = new_id

    def get_ip(self):
        return self._ip

    def set_ip(self, new_ip):
        self._ip = new_ip

    def get_status(self):
        return self._status

    def set_status(self, status):
        if isinstance(status, bool):
            self._status = status

    def get_alias(self):
        return self._alias

    def set_alias(self, new_alias):
        self._alias = new_alias

    def get_on_timer(self):
        return self._on_time

    def set_on_timer(self, new_time):
        if isinstance(new_time, int):
            self._on_time = new_time

    def get_off_timer(self):
        return self._off_time

    def set_off_timer(self, new_time):
        if isinstance(new_time, int):
            self._off_time = new_time


class LedBulb(Node):
    _color = 0x00
    _led = "off"

    def __init__(self, id, ip):
        super(LedBulb, self).__init__(id, ip)

    def get_json(self):
        # get the superclass json fields
        json = super(LedBulb, self).get_json()

        # update with local fields
        json.update({
            NodeFields.LED.value: self._led,
            NodeFields.COLOR.value: self._color
        })

        return json

    def set_from_json(self, new_json):
        for key, val in new_json.items():
            if key == NodeFields.LED:
                self.set_led(val)
            elif key == NodeFields.COLOR:
                self.set_color(val)
            else:
                # ignore json field
                pass

    def get_type(self):
        return "led bulb"

    def set_color(self, new_col):
        self._color = int(new_col, 16)

    def get_color(self):
        return self._color

    def toggle_led(self):
        if self._led == NodeCommands.ON:
            self._led = NodeCommands.OFF
        else:
            self._led = NodeCommands.ON

    def get_led(self):
        return self._led

    def set_led(self, led):
        self._led = led


class Sensor(Node):
    _sensor = 0

    def __init__(self, id, ip):
        super(Sensor, self).__init__(id, ip)

    def get_type(self):
        return "sensor"

    def get_value(self):
        return self._sensor

    def set_value(self, new_value):
        self._sensor = new_value
