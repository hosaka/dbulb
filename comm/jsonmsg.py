# perhaps should be renamed to avoid confusion
__author__ = 'march'
import json


class JsonHandler():
    parse = json

    def __init__(self):
        pass

    def decode(self, json_msg):
        # create a sting from a json
        return self.parse.dumps(json_msg, sort_keys=True)

    def encode(self, raw_msg):
        # create a json from a string
        return self.parse.loads(raw_msg)

    def pretty(self):
        pass