import datetime
import time
import csv
import itertools
import math
import numpy as np
import matplotlib.pyplot as plt

from util.text import PlotterMsg, PlotterCmd
from util import loggers

__author__ = 'march'

# Sleep as Android has 15 data fields before the timed data is listed
SKIP = 15


class Plotter():
    # message logger
    log = loggers.Logger(PlotterMsg.PLOTTER.value)

    def __init__(self, fname):
        self.log.info("{}: {}...".format(PlotterMsg.PARSING, fname))

        # load the cvs file
        try:
            with open(fname, 'r', newline='') as csvfile:
                self.data = [i for i in csv.reader(csvfile, delimiter=',', quotechar='"')]
        except FileNotFoundError:
            self.log.error("file not found: {}".format(fname))

        # times = []
        # for t in data[0][skip_col:]:
        #     if t not in "Event":
        #         times.append(datetime.datetime.strptime(t, "%H:%M"))
        # self.time_data = np.asarray(times)
        # self.acc_data = np.asarray(data[1][skip_col:len(times)+skip_col])

        # self.acc_data = np.genfromtxt(file, delimiter=',', skiprows=1, dtype=float, converters={1: strip_quotes})

    def plot(self, graph_type):

        fig = plt.figure()
        plt.style.use('ggplot')

        # attach some text labels
        def autolabel(rects):
            for rect in rects:
                height = rect.get_height()
                plt.text(rect.get_x() + rect.get_width() / 2., 1.05 * height, '%f' % float(height), ha='center', va='bottom')

        # sleep/accelerometer plot
        if graph_type in PlotterCmd.SLEEP:

            # remove empty fields from the data
            sleep_data = self.remove_empty_data(self.data)

            # separate odd/even values
            accel_data = sleep_data[1::2]
            time_data = sleep_data[0::2]

            # plot and save each data pair
            for e in range(0, len(accel_data)):
                #fig = plt.figure()
                times = np.asarray([datetime.datetime.strptime(t, "%H:%M") for t in time_data[e]])
                accel = np.asarray([t for t in accel_data[e]])

                # plot data
                ax = fig.add_subplot(1, 1, 1)
                ax.set_title("sleep measurements")

                ax.set_xlabel("time")
                ax.set_ylabel("accelerometer")
                ax.plot(times, accel, label="sleep data")
                # plt.savefig('sleep/plots/sleep_{}'.format(e), bbox_inches='tight')

            # # create numpy arrays from read data
            # time_data = np.asarray([datetime.datetime.strptime(t, "%H:%M") for t in self.data[0][SKIP:] if t not in "Event"])
            # acc_data = np.asarray([t for t in self.data[1][SKIP:len(self.time_data)+SKIP]])

        # deep sleep percentage plot
        elif graph_type in PlotterCmd.DEEP:

            # groups on the bar, using and not using the light bulb
            n_groups = 2
            index = np.arange(n_groups)

            # aggregate the deep sleep percentages when using light bulb
            bulb_deep_perc = []
            for i in self.data[:10]:
                bulb_deep_perc.append(float(i[12]))

            bulb_deep_average = 0
            for p in bulb_deep_perc:
                bulb_deep_average += p

            bulb_deep_average /= len(self.data)/2  # TODO: do not hardcode!

            # do the same for the remaining 10 entries when not using the light bulb
            nobulb_deep_perc = []
            for i in self.data[10:]:
                nobulb_deep_perc.append(float(i[12]))

            nobulb_deep_average = 0
            for p in nobulb_deep_perc:
                nobulb_deep_average += p

            nobulb_deep_average /= len(self.data)/2  # TODO: do not hardcode!

            # all averages
            deep_averages = (bulb_deep_average, nobulb_deep_average)

            # standard deviations
            bulb_variance = 0
            for n in bulb_deep_perc:
                bulb_variance += (n - bulb_deep_average)**2
            bulb_variance /= 10

            nobulb_variance = 0
            for n in nobulb_deep_perc:
                nobulb_variance += (n - nobulb_deep_average)**2
            nobulb_variance /= 10

            deep_deviation = (math.sqrt(bulb_variance), math.sqrt(nobulb_variance))

            # graph setup
            bar_width = 0.8
            opacity = 0.4

            # sleep states
            deep_sleep = plt.bar(index, deep_averages, bar_width,
                             alpha=opacity,
                             yerr=deep_deviation,
                             color='r',
                             label='deep sleep')

            # plot data
            plt.title('average state length')
            plt.xlabel('difference of {:.2f}%'.format((bulb_deep_average - nobulb_deep_average)*100))
            plt.ylabel('% / 100')
            plt.xticks(index + bar_width, ('using dbulb', 'without dbulb'))
            plt.legend()
            plt.tight_layout()

            autolabel(deep_sleep)

        # detailed sleep states plot
        elif graph_type in PlotterCmd.STATES:

            # create numpy arrays from read data
            # event_data = np.asarray(t for t in self.data[1])

            # self.time_data = np.asarray([datetime.datetime.strptime(t, "%H:%M") for t in self.data[0][SKIP:] if t not in "Event"])
            # self.acc_data = np.asarray([t for t in self.data[1][SKIP:len(self.time_data)+SKIP]])

            # TODO:
            # splits for events and epoch time, measure the start/end delta to see how often the events occurred
            # this can be used to plot the average event occurrence across each record
            events = []
            times = []
            for e in self.data[1]:
                # parse light/rem/deep
                event = e.split('-')
                events.append(event[0])
                # epoch is stored in milliseconds, divide the value by 1000 to get the correct format
                times.append(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(event[1]) / 1000)))

            # with and without the bulb groups
            n_groups = 2
            index = np.arange(n_groups)

            # graph setup
            bar_width = 0.3
            opacity = 0.4

            # the amount of elements in the tuples should correspond to the amount of groups
            # in this case, each tuple should have 2 elements for w/ and w/o bulb
            light_sleep_durations = (20, 36)
            rem_cycle_durations = (14, 18)
            deep_sleep_durations = (10, 15)

            # sleep states
            light_sleep = plt.bar(index, light_sleep_durations, bar_width,
                             alpha=opacity,
                             color='b',
                             label='light sleep')

            rem_cycle = plt.bar(index + bar_width, rem_cycle_durations, bar_width,
                             alpha=opacity,
                             color='g',
                             label='rem cycle')

            deep_sleep = plt.bar(index + bar_width*2, deep_sleep_durations, bar_width,
                             alpha=opacity,
                             color='r',
                             label='deep sleep')

            # plot data
            plt.title('average sleep states length')
            #plt.xlabel('difference of {:.2f}%'.format((bulb_deep_average - nobulb_deep_average)*100))
            plt.ylabel('state average duration %')
            plt.xticks(index + bar_width, ('using dbulb', 'without dbulb'))
            plt.legend()
            plt.tight_layout()

        # state changes amount with or without the bulb
        elif graph_type in PlotterCmd.CHANGES:
            # TODO: read the event transitions frequency (deep sleep only?)
            pass

        elif graph_type in PlotterCmd.ASLEEP:

            # remove empty fields from the data
            sleep_data = self.remove_empty_data(self.data)


            # separate all the dates jazz and find deltas for the times
            # yeah I know it's pretty horrible but i don't have time
            asleep_delta = []
            for event in sleep_data:
                asleep_delta.append(
                    datetime.datetime.fromtimestamp(int(int(event[1].split('-')[1]) / 1000))
                    -
                    datetime.datetime.strptime(event[0], "%d/%m/%Y %H:%M:%S")
                )

            # local datetime average function
            def avg_time(times):
                avg = 0
                for elem in times:
                    avg += elem.seconds + 60 * elem.minute + 3600 * elem.hour
                avg /= len(times)
                rez = str(avg/3600) + ' ' + str((avg % 3600)/60) + ' ' + str(avg % 60)
                return datetime.datetime.strptime(rez, "%H %M %S")

            # average timedelta for bulb and no bulb sessions
            # store the time in seconds, since matplotlib doesn't support datetime objects
            average_delta = ((sum(asleep_delta[:10], datetime.timedelta(0)) / 10).total_seconds(),
                             (sum(asleep_delta[10:], datetime.timedelta(0)) / 10).total_seconds())

            # standard deviations
            bulb_variance = 0
            for n in asleep_delta[:10]:
                bulb_variance += (n.total_seconds() - average_delta[0])**2
            bulb_variance /= 10

            nobulb_variance = 0
            for n in asleep_delta[10:]:
                nobulb_variance += (n.total_seconds() - average_delta[1])**2
            nobulb_variance /= 10

            asleep_deviation = (math.sqrt(bulb_variance), math.sqrt(nobulb_variance))

            # plot
            index = np.arange(2)

            # graph setup
            bar_width = 0.3
            opacity = 0.2

            bulb = [t.total_seconds() for t in asleep_delta[:10]]
            nobulb = [t.total_seconds() for t in asleep_delta[10:]]

            plt.plot(bulb, 'r--', label="using the bulb")
            plt.plot(nobulb, 'b--', label="not using the bulb")

            # sleep states
            asleep_time = plt.bar(index, average_delta, bar_width,
                             alpha=opacity,
                             color='r',
                             label='average')

            # plot data
            plt.title('time takes to enter the light sleep state')
            plt.xlabel('average difference of {:.2f}% or {:.2f} seconds'.format((average_delta[1]-average_delta[0])/average_delta[1]*100,
                                                                                (average_delta[1]-average_delta[0])))
            plt.ylabel('seconds lapsed')
            plt.xticks(index + bar_width, ('w/ dbulb', 'w/o dbulb'))
            plt.legend()
            plt.tight_layout()

            autolabel(asleep_time)

            # plot and save each data pair
            #times = np.asarray([datetime.datetime.strptime(t, "%d/%m/%Y %H:%M") for t in start_data])
            #accel = np.asarray([t for t in accel_data])

        # incorrect graph type
        else:
            self.log.error("{}: {}".format(PlotterMsg.UNKNOWN, graph_type))

    def remove_empty_data(self, data):
        new_data = []
        for entry in data:
            new_data.append(list(filter(lambda x: x != '', entry)))

        return new_data

    def save_image(self, fname):
        # save plotted image, remove extra space around the graph
        try:
            plt.savefig(fname, bbox_inches='tight')
        except FileNotFoundError:
                self.log.error("no such file or directory: {}".format(fname))

    def show(self):
        # display the image using the matplotlib viewer
        plt.show()
        pass
