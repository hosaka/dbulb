import threading
import socket

from control import autopilot
from control import node
from comm import jsonmsg

from util import loggers
from util.parsing import NodeCommandParser, PresetCommandParser
from util.text import *


__author__ = 'march'

CLIENT_PORT = 3912


class UDPClient:
    """Node UDP client used for message sending
    """

    def __init__(self, nodes, to_node):
        self.nodes = nodes
        self.to_node = to_node

        # message logger
        self.log = loggers.Logger(ServerMsg.UDP_CLNT.value)

    def send(self, args):
        # open UDP socket
        udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # construct node json message
        json_parser = jsonmsg.JsonHandler()
        raw = '{'

        for cmd in vars(args):
            # control actions
            if cmd == NodeCommands.ACTION:
                if args.action == NodeCommands.COLOR:
                    # append the color
                    raw += '"{0}":"{1}"'.format(NodeFields.COLOR, args.color)
                else:
                    # append the led status
                    raw += '"{0}":"{1}"'.format(NodeFields.LED, args.action)
                break
            # alias
            elif cmd == NodeCommands.ALIAS:
                nd = self.nodes.get_by_alias(args.alias)
                if nd is None:
                    self.to_node.set_alias(args.alias)
                else:
                    self.log.error("{}: {}".format(ErrorMsg.E_ALIAS_EXISTS, nd.get_id()))
                break
            else:
                # ignore the rest of the fields
                pass

        # close json
        raw += '}'

        # check json validity
        try:
            json = json_parser.encode(raw)

            # only discard empty messages
            if len(json) != 0:
                # update the node: all the changes will be unpacked and set for the node
                self.to_node.set_from_json(json)

                # send UDP message back
                udp_socket.sendto(bytes("{}".format(raw), "UTF-8"), (self.to_node.get_ip(), CLIENT_PORT))
                self.log.debug("{}: {}".format(ServerMsg.MSG_SENT, raw))
            else:
                self.log.debug("{}: {}".format(ServerMsg.MSG_DISC, raw))

        except ValueError:
            self.log.error("{}: {}".format(ServerMsg.JSON_INV, raw))

        # TODO: listen for ACK from the node


class UDPServer(threading.Thread):
    """Node UDP server listening for messages

    This class operates on messages coming from the UDP Handler
    Filtering through fields, modifying individual Nodes.
    """

    def __init__(self, ip, port, nodes, lock):
        self.ip = ip
        self.port = port
        self.nodes = nodes
        self.lock = lock

        # stop flag to control thread loop
        self._stop_flag = threading.Event()

        # message logger
        self.log = loggers.Logger(ServerMsg.UDP_HDLR.value)
        super(UDPServer, self).__init__()

    def run(self):
        try:
            udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            udp_socket.bind((self.ip, self.port))
        except OSError:
            self.log.error("{}: {}:{}".format(ErrorMsg.E_SOCK_USE, self.ip, self.port))
            udp_socket.close()
            self._stop_flag.set()

        # json parser
        parser = jsonmsg.JsonHandler()

        while not self._stop_flag.isSet():
            # add a socket timeout to allow thread to join
            udp_socket.settimeout(.2)

            try:
                data, address = udp_socket.recvfrom(1024)
                data = data.decode("UTF-8").strip()

                # process the json data
                if data.startswith('{"') and data.endswith('"}'):

                    # parse json
                    json_msg = parser.encode(data)
                    self.log.debug("{}: {}".format(ServerMsg.JSON_RCV, json_msg))

                    # check ID field in json
                    if NodeFields.ID in json_msg:
                        node_id = int(json_msg[NodeFields.ID], 16)  # remove the typecast if using hex for input

                        try:
                            self.lock.acquire()

                            # check nodes in table
                            if self.nodes.exists(node_id):
                                self.log.debug("{}: {}".format(ServerMsg.NODE_UPD, node_id))

                                # get existing node
                                nd = self.nodes.get(node_id)

                            else:
                                self.log.debug("{}: {}".format(ServerMsg.NODE_NEW, node_id))

                                # create a new node
                                # TODO: decide on the nodes type according to received message, maybe {"type":"led"}
                                nd = node.LedBulb(node_id, address[0])
                                nd.set_status(True)

                                # add to the nodes table
                                self.nodes.add(nd)

                                print("new node with id {} has connected from {}".format(node_id, address[0]))

                            # update other fields from the rest of the json message
                            del json_msg[NodeFields.ID]
                            if json_msg:
                                nd.set_from_json(json_msg)

                        finally:
                            self.lock.release()
                else:
                    # print plain text
                    self.log.debug("{}: {}".format(address[0], data))

                # send an ACK back to client
                udp_socket.sendto(bytes(ServerMsg.MSG_ACK, "UTF-8"), address)

            except socket.timeout:
                continue

            except Exception as e:
                self.log.error("{}: {}".format(ErrorMsg.E_MSG_RCV, e))

    def join(self, timeout=None):
        self._stop_flag.set()
        super(UDPServer, self).join(timeout)


class NodeServer(threading.Thread):
    """Main node server control thread
    """

    # data queue contains some messages
    # self.sched_queue = queue.Queue()

    # mutex lock
    lock = threading.RLock()

    # even scheduler
    scheduler = autopilot.Scheduler()

    # message logger
    log = loggers.Logger(ServerMsg.UDP_SRV.value)

    # presets table
    presets = autopilot.PresetTable()

    def __init__(self, addr, port):
        # server parameters
        self.addr = socket.gethostbyname(socket.gethostname()) if addr == "localhost" else addr
        self.port = port

        # node table
        self.nodes = node.NodeTable()

        # stop flag to control thread loop
        self._stop_flag = threading.Event()

        # udp server
        self.udp_srv = UDPServer(self.addr, self.port, self.nodes, self.lock)
        super(NodeServer, self).__init__()

    def run(self):
        # start the udp server
        self.udp_srv.start()

        # start scheduler
        self.scheduler.start()

        # add dummy node
        debug_node = node.LedBulb(0, "192.168.0.15")
        debug_node.set_alias("dbg")
        self.nodes.add(debug_node)

        # process the scheduler queue
        # while not self._stop_flag.isSet():

    def send(self, msg):
        try:
            # parse the arguments beforehand
            msg_parser = NodeCommandParser()
            args = msg_parser.parse(msg)

            # try to fetch the node
            nd = self.nodes.get_by_id_or_alias(args.id)
            if nd is not None:

                # delayed actions and timed actions only need to be scheduled
                if (hasattr(args, "delay") and args.delay is not None) or \
                        (hasattr(args, "timer") and args.timer is not None):

                    # schedule the actions if any
                    self.schedule(nd.get_id(), args)

                # otherwise start a udp client and send the arguments to the node
                else:
                    udp_cli = UDPClient(self.nodes, self.nodes.get(nd.get_id()))
                    udp_cli.send(args)
            else:
                self.log.warn("{}: {}".format(ErrorMsg.E_NO_NODE, args.id))

        except SystemExit:
            # we want to prevent the sys.exit so we catch the exception here
            pass

    def status(self):
        """Display server status and all connected nodes.
        """
        if not self.nodes.is_empty():
            print("-----{} {}:{}-----".format("serving", self.addr, self.port))
            print("-----{}-----\n".format(UIMsg.NODE_LIST))

            for item in self.nodes:
                nd = self.nodes.get(item)
                print("id: {:4} | ip: {:13} | led: {:3} | color: {:6} | alias: {:8} | type: {}".format
                (
                    nd.get_id(), nd.get_ip(), nd.get_led(), hex(nd.get_color()), nd.get_alias(), nd.get_type())
                )
        else:
            self.log.warn(ErrorMsg.E_NO_CONNECTED.value)

    def node_info(self, node):
        """Display individual node information.
        :param node: node id or alias to display the info for
        """
        print("-----{}-----".format(UIMsg.NODE_INFO))
        nd = self.nodes.get_by_id_or_alias(node)
        if nd is not None:
            self.node_print(nd)
        else:
            self.log.warn("{}: {}".format(ErrorMsg.E_NO_NODE, node))

    def node_print(self, nd):
        # fetch the json from the node and iterate through fields
        json = nd.get_json()
        for field, value in sorted(json.items()):
            print("{0:10}: {1}".format(field, value))

    def node_delete(self, node):
        """Delete the node from the table of connected elements.
        :param node: node id or alias to delete
        """
        nd = self.nodes.get_by_id_or_alias(node)
        if nd is not None:
            self.nodes.remove(nd.get_id())
        else:
            self.log.warn("{}: {}".format(ErrorMsg.E_NO_NODE, node))

    def schedule(self, node_id, args):

        # create a new timed or delay action for the scheduler
        if args.timer is not None:
            action = autopilot.TimerAction(args.timer, node_id, args.action)
        else:
            action = autopilot.DelayAction(args.delay, node_id, args.action)

        # add parameters if any
        if hasattr(args, "color"):
            action.add_param(args.color)

        # pass the action to the scheduler thread
        self.scheduler.add_action(action, self.send)

    def scheduler_info(self):
        """Display scheduler tasks information
        """
        if not self.scheduler.empty():
            print("-----{}-----".format("list of scheduled commands"))
            for event in self.scheduler:
                print("node id    : {}".format(event.get_node_id()))
                print("command    : {}".format(event.get_cmd()))
                print("add time   : {}".format(event.get_add_time()))
                print("start time : {}".format(event.get_exec_time()))
                print("--------------------------------")
        else:
            self.log.warn(ErrorMsg.E_SCHED_EMPTY.value)

    def preset(self, msg):
        try:
            # handle preset messages
            msg_parser = PresetCommandParser()
            args = msg_parser.parse(msg)
            print(vars(args))

        except SystemExit:
            # we want to prevent the sys.exit so we catch the exception here
            pass

    def preset_info(self):
        if not self.presets.empty():
            for id in self.presets:
                pres = self.presets.get_preset(id)
                print(str(pres.get_id())+" "+pres.get_action())

    def shutdown(self):
        # first wait for UDPServer to finish
        self.udp_srv.join()

        # shutdown the scheduler
        self.scheduler.join()

        # shutdown server thread
        self.join()

    def is_alive(self):
        return not self._stop_flag.isSet()

    def join(self, timeout=None):
        self._stop_flag.set()
        super(NodeServer, self).join(timeout)
