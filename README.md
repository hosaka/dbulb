dbulb
=====

[![Join the chat at https://gitter.im/alex-march/dbulb](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/alex-march/dbulb?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

IP Connected Light Bulb using ESP8266 WiFi module.

[LEMME BUILD IT NOW!](#build-and-usage)

Overview
--------
[url_espressif]: https://espressif.com/en/products/esp8266/
[url_nodemcu]: https://github.com/nodemcu/nodemcu-firmware

This project is an attempt to create a python daemon for controlling multiple IoT devices, such as
light bulbs, sensors, etc. all communicating via [Espressif ESP8266][url_espressif] wifi modules.
The application provides an interactive user interface where the connected modules can be controlled,
provide some automated tasks and presets (such as timed actions or controls based on the current user location/timezone)

The project originally was going to use Teensy board to drive the ESP module, shortly after the start an official SDK 
has been released by Espressif, allowing developers to program the chip on the module itself with custom applications. 
This would be a preferred way to develop the firmware for this project. The state of the SDK and the Toolchain however, 
still lacks the stability and a uniform way of development.

Another firmware has been released by [NodeMCU][url_nodemcu] which allows the user applications to be written in simple 
LUA scripts. This version is also included in this project and **currently in use**.

### Messaging
This project utilises JSON format for network messages and expects the same format back from the ESP modules.
Upon server startup, the app creates a local UDP server to continuously listen on for incoming messages.
Once the server receives the JSON message from the module, the module is stored internally for future communication.

The user can control connected modules by issuing commands on the interactive cli, for example toggling an LED or
setting its color. The server then creates a UDP client which broadcasts the message directly to the connected module.
Each connected module is independent and once the message has being received, the module will act accordingly.

### WiFi Module
The ESP should be configured on the same network as the server prior to making any connections. Once the module is powerd up, it'll attempt to connect to the specified SSID and will search for the dbulb server on your network.

### Build and Usage
[url_esplorer]: https://github.com/4refr0nt/ESPlorer
[url_luatool]: https://github.com/4refr0nt/luatool
1. Get the latest [NodeMCU firmware][url_nodemcu]
2. Upload the files from [lua firmware](esp8266/lua) using [luatool][url_luatool] or [ESPlorer][url_esplorer]
3. Launch dbulb using Python 3.1+ `python dbulb.py`
4. Type `start` to get the local server going
5. Type `help` for the list of available commands
6. Each command has its own help info `node 1 power -h`
7. A lot of commands have shorter versions, try `n 1` or `i light` for example
8. Hack the code to your hearts content and let me know if you made something awesome! :)

### TODO
#### Software
* Creating presets (based on day, weekend, timezone or anything else)
* Color fading and speed adjustment

#### Firmware
* Ability to reconfigure SSID/PWD on the module without firmware re-flashing
* C only version of the firmware built on the SDK

### Completed
* Firmware handles quick and multiple requests
* Plotters for Sleep as Android data
* Presets API
* Deleting active nodes
* Various time units for timer 
* Timed and delayed actions
* Alias handling
* Color commands
* NodeMCU firmware
* Robust input validation
* Power commands
* UDP Client
* JSON parsing
* Multiple Nodes data structure
* Node structure
* UDP Server
* Interactive CLI
* Argument parsing and help
