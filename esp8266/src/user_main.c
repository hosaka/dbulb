/******************************************************************************
 *  Dbulb application main routine
 *  author: march
******************************************************************************/

// SDK
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "user_interface.h"

// USER
// #include "udp_server.h"
#include "user_config.h"
// #include "driver/uart.h"

#define procTaskPrio        0
#define procTaskQueueLen    1

os_event_t procTaskQueue[procTaskQueueLen];
static void procTask(os_event_t *events);
static volatile os_timer_t some_timer;

// init function
void ICACHE_FLASH_ATTR user_init()
{
    //system_os_task(procTask, procTaskPrio, procTaskQueue, procTaskQueueLen);
    //system_os_post(procTaskPrio, 0, 0 );

    const char ssid[32] = "";
    const char password[32] = "";

    struct station_config stationConf;

    wifi_set_opmode( STATION_MODE );

    os_memcpy(&stationConf.ssid, ssid, 32);
    os_memcpy(&stationConf.password, password, 32);

    wifi_station_set_config(&stationConf);
    wifi_station_connect();

    // init network
    // init UART interface
    // uart_init(115200, 115200);
}

// void some_timerfunc(void *arg)
// {
//     //Do blinky stuff
//     if (GPIO_REG_READ(GPIO_OUT_ADDRESS) & BIT2)
//     {
//         //Set GPIO2 to LOW
//         gpio_output_set(0, BIT2, BIT2, 0);
//     }
//     else
//     {
//         //Set GPIO2 to HIGH
//         gpio_output_set(BIT2, 0, BIT2, 0);
//     }
// }

// // idle task
// static void ICACHE_FLASH_ATTR user_procTask(os_event_t *events)
// {
//     os_delay_us(10);
// }

// // led blink example
// void ICACHE_FLASH_ATTR led_blink()
// {
//     // Initialize the GPIO subsystem.
//     gpio_init();

//     //Set GPIO2 to output mode
//     PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);

//     //Set GPIO2 low
//     gpio_output_set(0, BIT2, BIT2, 0);

//     //Disarm timer
//     os_timer_disarm(&some_timer);

//     //Setup timer
//     os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, NULL);

//     //Arm the timer
//     //&some_timer is the pointer
//     //1000 is the fire time in ms
//     //0 for once and 1 for repeating
//     os_timer_arm(&some_timer, 1000, 1);

//     //Start os task
//     system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
// }
