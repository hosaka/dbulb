/******************************************************************************
 *  UDP Server Interface
 *  author: march
******************************************************************************/

// SDK
//#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "ip_addr.h"
#include "espconn.h"
#include "user_interface.h"

// USER
#include "user_config.h"
#include "udp_server.h"

// connection control block
static struct espconn *pUdpServer;

LOCAL void ICACHE_FLASH_ATTR
udpserver_recv(void *arg, char *pusrdata, unsigned short length)
{
    // call back function
}

LOCAL void ICACHE_FLASH_ATTR
tcp_con_cb(void *arg){
    // connect call back
}

LOCAL void ICACHE_FLASH_ATTR
tcp_discon_cb(void *arg, char *pusrdata, unsigned short length){
    // disconnect call back
}

void ICACHE_FLASH_ATTR tcp_connect(ip_addr_t *ip) {
  struct espconn *pespconn = (struct espconn *)os_zalloc(sizeof(struct espconn));
  pespconn->type = ESPCONN_TCP;
  pespconn->state = ESPCONN_NONE;
  pespconn->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
  pespconn->proto.tcp->local_port = espconn_port();
  pespconn->proto.tcp->remote_port = 80;

  *((uint8 *) &pespconn->proto.tcp->remote_ip)     = (ip->addr >> 24) & 0xff;
  *((uint8 *) &pespconn->proto.tcp->remote_ip + 1) = (ip->addr >> 16) & 0xff;
  *((uint8 *) &pespconn->proto.tcp->remote_ip + 2) = (ip->addr >> 8) & 0xff;
  *((uint8 *) &pespconn->proto.tcp->remote_ip + 3) = (ip->addr) & 0xff;

  espconn_regist_connectcb(pespconn, tcp_con_cb);
  //espconn_regist_reconcb(pespconn, tcp_dicon_cb);
  int8_t res = espconn_connect(pespconn);
  os_printf("espconn_connect() returned %d", res);
}

void ICACHE_FLASH_ATTR
udp_init(void)
{
    // allocate memory for the control block
    pUdpServer = (struct espconn *)os_zalloc(sizeof(struct espconn));

    // fill memory with 0 bytes
    ets_memset( pUdpServer, 0, sizeof( struct espconn ) );

    // create the connection from block
    espconn_create( pUdpServer );

    // set connection to UDP
    pUdpServer->type = ESPCONN_UDP;

    // alloc memory for the udp object in the server context and assign port
    pUdpServer->proto.udp = (esp_udp *)os_zalloc(sizeof(esp_udp));
    pUdpServer->proto.udp->local_port = CONFIG_UDP_PORT;

    // register the callback function for receiving data
    espconn_regist_recvcb(pUdpServer, udpserver_recv);

    if( espconn_create( pUdpServer ) )
    {
        while(1);
    }
}