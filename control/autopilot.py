import threading
import time
import datetime
import sched

from util import loggers
from util.text import SchedulerMsg, NodeCommands

__author__ = 'march'


class PresetTable:
    def __init__(self):
        self._presets = {}

    def __iter__(self):
        for p in sorted(self._presets):
            yield p

    def add_preset(self, preset):
        self._presets[preset.get_id()] = preset

    def get_preset(self, _id):
        return self._presets[_id]

    def empty(self):
        return not self._presets


class Preset:
    def __init__(self, id, action):
        self._id = id
        self._action = action

        # indicate whether the preset is runnable
        self._active = False

    def get_id(self):
        return self._id

    def set_action(self, new_action):
        self._action = new_action

    def get_action(self):
        return self._action

    def add_to_scheduler(self, sched):
        pass

    def is_active(self):
        return self._active

    def activate(self):
        self._active = True

    def deactivate(self):
        self._active = False


class SchedulerAction:
    def __init__(self, timer, node_id, cmd):
        self._node_id = node_id
        self._timer = int(timer)
        self._cmd = cmd

        # note the added time and calculate future timestamp
        self._add_timestamp = time.time()

        # parse the action arguments
        self._args = self.parse_args()

    def parse_args(self):
        args = [str(self._node_id)]
        if self._cmd == "on" or self._cmd == "off":
            args.extend(["power", self._cmd])
        elif self._cmd == "color":
            args.append("color")
        return args

    def add_param(self, param):
        self._args.append(param)

    def get_args(self):
        return self._args

    def get_node_id(self):
        return self._node_id

    def get_cmd(self):
        return self._cmd

    def get_add_time(self):
        """Return the formatted addition time of  the actoin

        :return: the time when action has been added
        """
        return datetime.datetime.fromtimestamp(self._add_timestamp).strftime("%H:%M:%S %Y-%m-%d")

    def get_exec_time(self):
        """Return formatted execution time of the action.

        The implementation of this method depends on the action type.
        """
        # exec time of delay action is add ts plus timer value
        pass

    def get_timer(self):
        return self._timer

    def get_prio(self):
        return 1
        #return self.prio if not None else 1


class DelayAction(SchedulerAction):
    def get_exec_time(self):
        return datetime.datetime.fromtimestamp(self._add_timestamp + self._timer).strftime("%H:%M:%S %Y-%m-%d")


class TimerAction(SchedulerAction):
    def get_exec_time(self):
        return datetime.datetime.fromtimestamp(self._timer).strftime("%H:%M:%S %Y-%m-%d")


class Scheduler(threading.Thread):
    def __init__(self):
        # stop flag to control thread loop
        self._stop_flag = threading.Event()

        # threaded scheduler
        self.sched = sched.scheduler(time.time, time.sleep)

        # message logging
        self.log = loggers.Logger(SchedulerMsg.SCHED.value)

        # keep track of the event actions
        self._events = []

        super(Scheduler, self).__init__()

    def __iter__(self):
        """Iterate a list of scheduled tasks
        """
        for event in self._events:
            yield event

    def add_action(self, action, function):
        """Add a new delayed SchedulerAction with a function callback.
        """
        # delay = in seconds, priority = the higher the less, function to callback, arguments tuple
        if isinstance(action, TimerAction):
            self.sched.enterabs(action.get_timer(), action.get_prio(), function, argument=(action.get_args(),))
        elif isinstance(action, DelayAction):
            self.sched.enter(action.get_timer(), action.get_prio(), function, argument=(action.get_args(),))
        else:
            self.log.error("invalid action type, this shouldn't have happened")

        self._events.append(action)
        print("schedule at: " + action.get_add_time())
        print("will run at: " + action.get_exec_time())
        self.log.info(SchedulerMsg.ACTION_NEW.value)

    def run(self):
        while not self._stop_flag.is_set():
            # TODO: check the presets queue for existing actions that need to run
            if not self.sched.empty():
                # TODO: scheduler doesn't exit when issuing quit, even with an empty task list
                self.sched.run()

    def empty(self):
        return self.sched.empty()

    def set_timezone(self):
        pass

    def get_timezone(self):
        pass

    def join(self, timeout=None):
        for event in self.sched.queue:
            self.sched.cancel(event)

        self._stop_flag.set()
        super(Scheduler, self).join(timeout)



