from control.node import Node

__author__ = 'march'

import unittest
from control import node


class NodesTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def test_create_new_node(self):
        nd = Node(1, "192.168.0.17")
        self.assertNotEqual(nd, None)

    def test_node_status(self):
        nd = Node(0x01, "192.168.0.4")
        # default node comes on offline
        self.assertEqual(nd.get_status(), False)

        # modify node status
        nd.set_status(True)
        self.assertEqual(nd.get_status(), True)

        # accept only online/offline strings
        nd.set_status("garbage")
        self.assertEqual(nd.get_status(), True)

    def test_node_id(self):
        new_id = 0x01
        nd = Node(new_id, "192.168.0.4")
        self.assertEqual(nd.get_id(), new_id)

        # only integer ids
        nd.set_id("garbage")
        self.assertEqual(nd.get_id(), new_id)

    def test_node_colors(self):
        nd = Node(0x01, "192.168.0.4")

        # getting current node colors
        cols = nd.get_color()
        self.assertEqual(cols, {'red': 0, 'green': 0, 'blue': 0})

        # setting new colors
        new_colors = {'red': 255, 'green': 255, 'blue': 255}
        nd.set_color(new_colors)
        self.assertEqual(nd.get_color(), new_colors)

    def test_node_timers(self):
        offtime, ontime = 100, 50
        nd = Node(0x01, "192.168.0.4")

        # set on/off timers
        nd.set_on_timer(ontime)
        nd.set_off_timer(offtime)

        self.assertEqual(nd.get_on_timer(), ontime)
        self.assertEqual(nd.get_off_timer(), offtime)

if __name__ == '__main__':
    unittest.main()
