-- config
srv_ip = "192.168.43.202"
srv_port = 3912

dofile("led.lua")

-- client callback
function udp_resp_cb(sck,data)
     if data == "ACK" then
          print(data)
     else
          print("NACK")
     end
end

-- server callback
function udp_recv_cb(sck,data) 
     local success, resp = pcall(cjson.decode, data)
     if not success then
        led_error_flash(3)
        -- udp_send("ERR")
     else
        led_parse_json(resp)
        udp_send("ACK")
     end
end

-- udp server and client
function udp_init()
     srv = net.createServer(net.UDP)    
     srv:on("receive",function(s,c)
          udp_recv_cb(s,c)
     end)
     srv:listen(srv_port)
end

function udp_send(msg)
     cl = net.createConnection(net.UDP)
     cl:on("receive",function(s,c)
          udp_resp_cb(s,c)
     end)
     cl:connect(srv_port, srv_ip)
     cl:send(msg)
end

-- inits
led_init()
udp_init()

-- send id message to client
udp_send('{"_id":"0x01"}')
