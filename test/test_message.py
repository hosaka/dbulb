import unittest
from comm import jsonmsg

__author__ = 'march'


class MessagingTest(unittest.TestCase):

    def setUp(self):
        pass

    def test_create_default_json_object(self):
        json_handle = jsonmsg.JsonHandler()

    def test_make_json_from_raw_string(self):
        # json handler is a wrapper around json library
        json_handle = jsonmsg.JsonHandler()

        # can encode the string to create a json dict
        msg = json_handle.encode('{"led":"on"}')
        self.assertEqual(msg, {'led': 'on'})

    def test_make_raw_string_from_json(self):
        json_handle = jsonmsg.JsonHandler()

        # can decode the json dict back to string before sending back
        msg = json_handle.decode({'led': 'off'})

        # note the space after the colon, it is necessary
        self.assertEqual(msg, '{"led": "off"}')

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
