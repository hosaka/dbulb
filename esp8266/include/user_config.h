#ifndef __USER_CONFIG_H__
#define __USER_CONFIG_H__

/* Server configuration */
//#include "udp_server.h"
#define SRV_SSID "ssid"
#define SRV_PASS "pwd"
#define SRV_DST_HOST "dbulb"
#define SRV_DST_IP "192.168.0.4"
#define SRV_DST_PORT "3912"

#define SRV_MSG_ACK "ACK"
#define SRV_MSG_NACK "NACK"

/* Device configuration */
// more types of devices can be added here
#include "driver/rgb_led.h"

#endif

