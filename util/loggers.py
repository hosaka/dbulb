import logging
from util.text import UIMsg, ServerMsg, SchedulerMsg

__author__ = 'march'

DEBUG = logging.DEBUG
INFO = logging.INFO
WARN = logging.WARN
ERROR = logging.ERROR


def set_loggers_level(new_level):
    # define format and level
    logging.basicConfig(level=new_level, format="%(name)s: %(message)s")


class Logger():
    def __init__(self, name):
        self.log = logging.getLogger(name)
        # self.log.setLevel(INFO)

    def debug(self, msg):
        self.log.debug(msg)

    def info(self, msg):
        self.log.info(msg)

    def warn(self, msg):
        self.log.warn(msg)

    def error(self, msg):
        self.log.error(msg)
