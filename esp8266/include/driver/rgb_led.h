#ifndef __RGB_LED_H__
#define __RGB_LED_H__

#include "gpio.h"

// LED pins
#define R_PIN 1
#define G_PIN 2
#define B_PIN 3

#endif