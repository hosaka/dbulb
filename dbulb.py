# import signal
import argparse

from comm import server
from util import loggers
from util.text import UICmd, UIMsg

__author__ = 'march'


class Main:
    # default server parameters
    # server_addr = "192.168.43.202"
    server_addr = "192.168.0.4"
    server_port = 3912

    # message logger
    log = loggers.Logger(UIMsg.UI.value)

    # server status flag
    _started = False

    # server object
    srv = None

    def interact(self, args):
        while True:
            try:
                cmd = input("> ").strip()
            except EOFError:
                # prevent Ctrl-D EOF
                cmd = UIMsg.HELP[0]

            if cmd in UICmd.STATUS:
                if self._started:
                    self.status(None)
                else:
                    self.log.warn(UIMsg.SRV_NOT_RUN.value)

            elif cmd.split(" ")[0] in UICmd.NODE:
                if self._started:
                    self.node(cmd.split(" "))
                else:
                    self.log.warn(UIMsg.SRV_NOT_RUN.value)

            elif cmd.split(" ")[0] in UICmd.INFO:
                if self._started:
                    self.node_info(cmd.split(" "))
                else:
                    self.log.warn(UIMsg.SRV_NOT_RUN.value)

            elif cmd in UICmd.SCHED:
                if self._started:
                    self.scheduler_info()
                else:
                    self.log.warn(UIMsg.SRV_NOT_RUN.value)

            elif cmd.split(" ")[0] in UICmd.PRESET:
                if self._started:
                    self.preset(cmd.split(" "))
                else:
                    self.log.warn(UIMsg.SRV_NOT_RUN.value)

            elif cmd in UICmd.HELP:
                self.help()

            elif cmd.split(" ")[0] in UICmd.DELETE:
                if self._started:
                    self.node_delete(cmd.split(" "))
                else:
                    self.log.warn(UIMsg.SRV_NOT_RUN.value)

            elif cmd in UICmd.START:
                if not self._started:
                    self.start(None)
                else:
                    self.log.warn(UIMsg.SRV_STARTED.value)

            elif cmd in UICmd.STOP:
                if self._started:
                    self.stop(None)
                    self._started = False
                else:
                    self.log.warn(UIMsg.SRV_STOPPED.value)

            elif cmd in UICmd.QUIT:
                if self._started:
                    self.stop(None)
                break
            elif cmd in UICmd.VER:
                self.version()
            else:
                print(UIMsg.PRE_HELP.value)

    def start(self, args):
        if args is None:
            self.server_port = int(input(UIMsg.SRV_PORT_IN + ": ").strip())
        else:
            self.server_port = args.port

        # starts server thread
        self.srv = server.NodeServer(self.server_addr, self.server_port)
        self.srv.start()
        self._started = self.srv.is_alive()
        self.log.info("{}: {}...".format(UIMsg.SRV_START, self.server_port))

    def stop(self, args):
        self.log.info(UIMsg.SRV_STOP.value)
        self.srv.shutdown()

    def status(self, args):
        # list connected nodes
        self.srv.status()

    def scheduler_info(self):
        self.srv.scheduler_info()

    def node(self, args):
        # remove node argument
        args.remove(args[0])

        # send the commands via server
        self.srv.send(args)

    def node_info(self, args):
        if len(args) == 2:
            # display node info from the server
            self.srv.node_info(args[1])
        else:
            self.log.warn(UIMsg.INFO_FORMAT.value)

    def node_delete(self, args):
        if len(args) == 2:
            if input("{}: {}? [y/N] ".format(UIMsg.NODE_DELETE, args[1])) in ("y", "yes"):
                # delete the node from the server
                self.srv.node_delete(args[1])
            else:
                self.log.warn(UIMsg.NODE_DEL_ABORT.value)
        else:
            self.log.warn(UIMsg.DEL_FORMAT.value)

    def preset(self, args):
        # remove preset argument
        args.remove(args[0])

        # call the server preset handling function
        self.srv.preset(args)

    def plot(self, args):
        from util import plotter

        # create the plotter and attempt to open the given filename
        plot = plotter.Plotter(args.filename)

        # TODO: stop on file error
        # plot the data using the specified type
        plot.plot(args.type)

        if args.out is not None:
            plot.save_image(args.out)
        else:
            plot.show()

    def version(self):
        print("v" + UIMsg.VER.value)

    def help(self):
        print(UIMsg.HELP.value)

    def check_file_extension(self, choices, fname):
        pass

    def __init__(self):
        # top-level parser and subparsers
        parser = argparse.ArgumentParser(prog="dbulb", description="available commands description:")
        subparsers = parser.add_subparsers(title="commands", description="description:",
                                                help="call with -h for more help")

        # optional args
        parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
        parser.add_argument("-V", "--version", help="display version and quit", action="store_true")

        # server controls
        parser_server = subparsers.add_parser("start", help="start new server")
        parser_server.add_argument("port", type=int, help="serve on port")
        parser_server.set_defaults(func=self.start)

        # parser_status = subparsers.add_parser("status", help="display server status")
        # parser_status.set_defaults(func=self.status)

        # plotter functions
        parser_plot = subparsers.add_parser("plot", help="plot raw sleep data")
        # TODO: add subparser for diagram types
        parser_plot.add_argument("type", type=str, help="type of diagram to plot")
        parser_plot.add_argument("filename", type=str, help="sleep data in CSV format")
        parser_plot.add_argument("-o", "--out", type=str, help="save png plot image instead of displaying",)
        parser_plot.set_defaults(func=self.plot)

        # parse the arguments
        args = parser.parse_args()

        # set verbosity if specified
        if args.verbose:
            loggers.set_loggers_level(loggers.DEBUG)
        if args.version:
            self.version()

        # if either of the control functions were specified call their functions
        if "func" in args:
            args.func(args)

        # run interactive mode
        self.interact(args)


# SIGINT handler
def int_handler(signum, frame):
    print("{}".format(UIMsg.SIGINT_MSG), end='')

# signal.signal(signal.SIGINT, int_handler)

if __name__ == "__main__":
    Main()
