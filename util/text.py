from enum import Enum
__author__ = 'march'

# a collection of strings enums that are used in the application
# either as commands or json fields


class NodeFields(str, Enum):
    ID = "_id"
    IP = "ip"
    LED = "led"
    COLOR = "color"
    ALIAS = "alias"
    ON_TIME = "on_time"
    OFF_TIME = "off_time"


class NodeCommands(str, Enum):
    POWER = "power"
    ON = "on"
    OFF = "off"
    COLOR = "color"
    ACTION = "action"
    DELAY = "delay"
    TIMER = "timer"
    ALIAS = "alias"


class ColorsText(str, Enum):
    # The amount of Text colors and Hex colors should match!
    RED = "red"
    GREEN = "green"
    BLUE = "blue"
    WHITE = "white"
    FLUX = "flux"


class ColorsHex(str, Enum):
    # The amount of Text colors and Hex colors should match!
    RED = "ff0000"
    GREEN = "00ff00"
    BLUE = "0000ff"
    WHITE = "ffffff"
    FLUX = "cb4b16"


class UICmd(tuple, Enum):
    HELP = ("help", "h", "?")
    START = ("start", "run")
    STOP = ("stop", "kill")
    STATUS = ("status", "stat", "s")
    INFO = ("info", "i")
    NODE = ("node", "n")
    DELETE = ("delete", "del", "d")
    SCHED = ("sched", "tasks", "t")
    PRESET = ("preset", "pres", "p")
    VER = ("version", "ver", "v")
    QUIT = ("quit", "exit", "q")


class PlotterCmd(str, Enum):
    SLEEP = "sleep",
    DEEP = "deep",
    STATES = "states",
    CHANGES = "changes"
    ASLEEP = "asleep"


class PlotterMsg(str, Enum):
    PLOTTER = "PLOT",
    PARSING = "parsing"
    UNKNOWN = "unknown diagram type"


class UIMsg(str, Enum):
    UI = "UI"
    VER = "0.1"
    PRE_HELP = "type help to see all commands"
    HELP = "-----available commands-----\n" \
           "start/stop       : control the main server\n" \
           "status           : print server stats and nodes\n" \
           "info id          : print info about a single node\n" \
           "node id command  : send commands to connected nodes\n" \
           "del  id          : delete node from the list of connected\n" \
           "tasks            : print info about scheduled tasks\n" \
           "preset command   : control and create presets for scheduler\n" \
           "help             : print this help message\n" \
           "quit             : shut server down and quit"

    SIGINT_MSG = "\ntype quit or exit to leave\n> "
    SRV_PORT_IN = "start on port"
    SRV_START = "serving on port"
    SRV_RUN = "server is running on"
    SRV_STARTED = "server is already started"
    SRV_NOT_RUN = "server is stopped"
    SRV_STOP = "stopping server..."
    SRV_STOPPED = "server already stopped"
    NODE_DELETE = "trying to delete node"
    NODE_DEL_ABORT = "not deleting anything"
    NODE_LIST = "list of connected nodes"
    NODE_INFO = "node info"
    INFO_FORMAT = "use format: info id -h"
    DEL_FORMAT = "use format: del id -h "


class ServerMsg(str, Enum):
    UDP_SRV = "UDP Server"
    UDP_CLNT = "UDP Client"
    UDP_HDLR = "UDP Handler"
    MSG_NEW = "MSG_NEW"
    MSG_SENT = "MSG_SENT"
    MSG_DISC = "MSG_DISCARDED"
    MSG_ACK = "ACK"
    JSON_RCV = "JSON_RCV"
    JSON_INV = "JSON_INVALID"
    NODE_NEW = "NODE_NEW"
    NODE_UPD = "NODE_UPD"


class ErrorMsg(str, Enum):
    C_UNKNOWN = "is not a valid color"
    C_NO_VALUE = "color name or hex value is required"
    E_SOCK_USE = "unable to bind, address already in use"
    E_MSG_RCV = "error receiving msg"
    E_INC_NODE_MSG = "incorrect node command"
    E_NO_CONNECTED = "no connected nodes"
    E_NO_NODE = "no node with id or alias"
    E_ALIAS_EXISTS = "alias already in use for node with id"
    E_SCHED_EMPTY = "scheduler has no active tasks"


class SchedulerMsg(str, Enum):
    SCHED = "SCHED"
    ACTION_NEW = "successfully scheduled new action"
